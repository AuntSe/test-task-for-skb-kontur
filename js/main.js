window.onload = function() {
	var cities = getJsonData();
	var cityField = document.getElementById("city");
	var dropDownList = document.getElementsByTagName("ul");
	var warning = document.getElementsByTagName("p");
	var cityForm = document.getElementsByTagName("form");
	var listItem = document.getElementsByTagName("li");
	var howMuchItems = 5;
	var correctValue = true;
	
	/*Города для опции "Избранные варианты". Впоследствии можно формировать этот массив любым 
		удобным способом. Количество городов в нем может быть любым.*/
		
	var popularCities = ["Москва", "Санкт-Петербург", "Екатеринбург", "Новосибирск"];
	
	cityField.addEventListener("focus", formOnFocus);
	cityField.addEventListener("keydown", ecsapeDefault);
	cityField.addEventListener("keyup", getCityValue);
	cityField.addEventListener("keyup", pressed);
	document.addEventListener("click", clickOnElements);
	
	function getCityValue(e) {
		if (e.which != 40 && e.which != 38 && e.which != 27 && e.which != 13 && e.which != 9) {
			var cityValue = cityField.value;
			var foundCities = [];
			unMakeWarning();
			correctValue = true;
		
			/* Нахождение совпадения набранного в поле с названиями городов в файле
				Регистр не учитывается, чтобы облегчить пользователю задачу: 
				совпадения находятся даже если он начал набирать название с маленькой буквы или не отключил CapsLock
				Совпадения ищутся не только в начале слова, но и после пробелов и дефисов, то есть, с начала каждого слова
				в названии города (опция "Поиск по вхождению"). 
				Функционал, при котором слово начинается с кавычек, и они не учитываются,
				не реализован, поскольку в заданном файле нет названий в кавычках, но он может быть реализован
				по аналогии с поиском совпадений после пробелов и дефисов добавлением нескольких строк кода.*/
			
			if(cityValue != ""){
				for(var i=0; i<cities.length; i++){
					var foundHyphen = findSymbol(cities[i].City, "-");
					var foundSpace = findSymbol(cities[i].City, " ");
				
					compareValues(cities[i].City, 0, cityValue.length, cityValue, foundCities);
				
					if(foundHyphen.length != 0) {
						for(var j=0; j<foundHyphen.length; j++) {
							compareValues(cities[i].City, foundHyphen[j]+1, cityValue.length, cityValue, foundCities);
						}	
					}
				
					if(foundSpace.length != 0) {
						for(var j=0; j<foundSpace.length; j++) {
							compareValues(cities[i].City, foundSpace[j]+1, cityValue.length, cityValue, foundCities);
						}
					}
				}
			}
		
			/* Формирование списка вариантов в зависимости от наличия или отсутсвия совпалений,
				а также от того, набрано ли что-то в поле или нет */
				
			dropDownList[0].style.display = ("block");
			if(foundCities.length != 0) {
				var listContent = "";
				var count=0;
				for(var i=0; i<howMuchItems; i++) {
					if(foundCities[i]) {
						var start = foundCities[i].toLowerCase().indexOf(cityValue.toLowerCase());
						if(start == 0) {
							listContent += "<li class='item'><span>" + foundCities[i].substr(start,cityValue.length) + 
								"</span>" + foundCities[i].substr(cityValue.length) + "</li>";
						}
						else {
							listContent += "<li class='item'>" + foundCities[i].substr(0,start) + 
								"<span>" + foundCities[i].substr(start, cityValue.length) + "</span>" +
								foundCities[i].substr(start+cityValue.length) + "</li>";
						}
						count+=1;
					}
				}
				if(foundCities.length>howMuchItems) {
					listContent += "<li class='smalltext hint'>Показано " + count + " из " + foundCities.length + " найденных городов. Уточните запрос, чтобы увидеть остальные"
				}
				dropDownList[0].innerHTML = listContent;
				listItem[0].setAttribute("class", "item highlited");
				dropDownList[0].onmouseenter = itemUnHover;
			}
			else {
				if(cityField.value) {
					dropDownList[0].innerHTML = "<li class='hint'>Ничего не найдено</li>";
					makeWarning();
					correctValue = false;
				}
				else {
					dropDownList[0].innerHTML = makeListOfCities("<li class='smalltext hint'>Популярные города</li>", popularCities);
				}
			}
		}
		for(var i=0; i<listItem.length; i++) {
			listItem[i].addEventListener("mouseenter", itemHover);
			listItem[i].addEventListener("mouseleave", itemUnHover);
		}
	}

	// Обработчик события клика по разным элементам страницы
	
	function clickOnElements(event) {
		if (event.target.className == "item" || event.target.className == "item highlited") {
			var toValue = event.target.innerHTML.replace("<span>","");
			toValue = toValue.replace("</span>","");
			cityField.value = toValue;
			event.target.style.backgroundColor = ("#f1f1f1");
			dropDownList[0].style.display = ("none");
		}
		else if (event.target.className == "smalltext hint") {
			cityField.focus();
		}
		else if (event.target.className == "hint") {
			cityField.focus();
			dropDownList[0].style.display = ("none");
		}
		else if (event.target.id == "city" || event.target.tagName == "label") {
			cityField.focus();
		}
		else {
			dropDownList[0].style.display = ("none");
			makeError();
		}
		// cityForm[0].submit();
	}

	// Когда фокус в поле
	
	function formOnFocus() {
		unMakeWarning();
		if(cityField.value) {
			this.select();
		}
		else {
			dropDownList[0].style.display = ("block");
			dropDownList[0].innerHTML = makeListOfCities("<li class='smalltext hint'>Популярные города</li>", popularCities);
		}
		for(var i=0; i<listItem.length; i++) {
			listItem[i].addEventListener("mouseenter", itemHover);
			listItem[i].addEventListener("mouseleave", itemUnHover);
		}
	}
	
	// Добавление стилей для элементов при необходимости выдать предупреждение или сообщени об ошибке
	
	function makeError() {
		if (!correctValue) {
			cityField.setAttribute("class", "error");
			warning[0].style.display = ("block");
			warning[1].style.display = ("none");
		}
	}
	
	function makeWarning() {
		cityField.setAttribute("class", "warning");
		warning[1].style.display = ("block");
		warning[1].style.color = ("#f69c00");
	}
	
	function unMakeWarning() {
		cityField.setAttribute("class", "");
		warning[0].style.display = ("none");
		warning[1].style.display = ("none");
	}
	
	// Создание списка вариантов
	
	function makeListOfCities(message, array) {
		var list = message;
		for (var i=0; i<array.length; i++) {
			list += "<li class='item'>" + array[i] + "</li>";
		}
		return list;
	}
	
	// Обработчики события наведении на элементы списка курсора мыши и ухода курсора с них
	
	function itemHover(event) {
		if (event.target.className == "item") {
			for(var i=0; i<listItem.length; i++) {
				if (listItem[i].className != "smalltext hint" && listItem[i].className != "hint") {
					listItem[i].setAttribute("class", "item");
				}
			}
			event.target.setAttribute("class", "item highlited");
		}
	}
	
	function itemUnHover(event) {
		if(event.target.className == "list" && listItem[0].className == "item highlited") {
			listItem[0].setAttribute("class", "item");
		}
		if (event.target.className == "item highlited") {
			event.target.setAttribute("class", "item");
		}
	}
	
	/* Нахождение всех позиций заданного символа в заданной строке.
		Получает строку и символ, возвращает массив с позициями символа
		или пустой массив, если символ не найден в строке */
		
	function findSymbol(string, symbol) {
		var pos = 0, foundPos = 0;
		var foundSymbol = [];
		while (true) {
			foundPos = string.indexOf(symbol, pos);
			if (foundPos == -1) {
				break;
			}
			else{
				pos = foundPos+1;
				foundSymbol.push(foundPos);
			}
		}
		return foundSymbol;
	}
	
	function compareValues(string, position, length, valueToCompare, array) {
		var comparedValue = string.substr(position,length);
		if(valueToCompare.toLowerCase() == comparedValue.toLowerCase()) {
			array.push(string);
		}
	}
	
	/* Сброс действия по умолчаеию при нажатии клавиши Escape: в случае с полем формы 
		отменяет отправку значения формы при нажатии клавиши*/
		
	function ecsapeDefault(e) {
		if(e.which == 13 || e.which == 9) {
			e.preventDefault();
		}
	}
	
	// Работа с клавиатурой (дополнительные опции)
	
	function pressed(e) { 
		switch (e.which) {
			case 9: //Tab
				dropDownList[0].style.display = ("none");
				makeError();
				cityField.blur();
				break;
			case 27: //Esc
				dropDownList[0].style.display = ("none");
				break;
			case 13: //Enter
				for(var i=0; i<listItem.length; i++) {
					if(listItem[i].className == "item highlited") {
						var toValue = listItem[i].innerHTML.replace("<span>","");
						toValue = toValue.replace("</span>","");
						cityField.value = toValue;
					}
				}
				cityField.blur();
				dropDownList[0].style.display = ("none");
				if (cityField.className == "warning") {
					cityField.setAttribute("class", "error");
					warning[0].style.display = ("block");
					warning[1].style.display = ("none");
				}
				break;
			case 38: //arrow up
				for(var i=1; i<listItem.length; i++) {
					if(listItem[i].className == "item highlited" && listItem[i-1].className != "smalltext hint"){
						listItem[i-1].className = ("item highlited");
						listItem[i].className = ("item");
						break;
					}
				}
				break;
			case 40: //arrow down
				var count = 0;
				for(var i=0; i<listItem.length; i++) {
					if(listItem[i].className == "item highlited"){
						count+=1;
						if(listItem[i+1].className != "smalltext hint"){
							listItem[i+1].className = ("item highlited");
							listItem[i].className = ("item");
							break;
						}
					}
				}
				if(count == 0){
					if(listItem[0].className != "smalltext hint" && listItem[0].className != "hint") {
						listItem[0].className = ("item highlited")
					}
					else {
						listItem[1].className = ("item highlited");
					}
				}
				break;
		}
    }
	
	// Получение данных из JSON файла
	
	function getJsonData(){
		var jqXHR = $.ajax({
			url: "kladr.json",
			async: false});
		return $.parseJSON(jqXHR.responseText);
	}	
}
